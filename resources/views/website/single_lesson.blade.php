@extends('website.layout.master')

@section('page_title')   {{$page_title}}  @endsection
@section('content')

    @include('website.layout.inner-header')

    <section class="tutori-course-single tutori-course-layout-3 page-wrapper pb-0">
        <div class="container">
            <div class="row d-flex justify-content-between mb-30">
                <div class="col-xl-10">
                    <div class="row">
                        <div class="col-12 mb-4">
                            <div class="course-thumbnail">
                                <img
                                    src="{{assetURLFile($content->image)}}"
                                    alt="{{$content->title}}"
                                    class="img-fluid w-100 fit-cover"
                                    style="max-height: 360px"
                                />
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="single-course-details mb-4">
                                <h4 class="course-title">نبذه عن الدرس</h4>
                                <div class="head-decorator head-decorator-sm mb-4"></div>
                                <p>
                                    {!! $content->short_description !!}
                                </p>
                            </div>
                            @include('website.includes.multiple_files')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-light py-4 pb-0">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="head mb-5">
                        <h4 class="fw-bold mb-3">
                            @if($content->type == 'lesson')
                                المهارات المكتسبة من هذا الدرس
                            @else
                                المهارات المكتسبة من هذه المحاضرة
                            @endif
                            <span class="text-muted fs-16 ms-2">( {{$content->skills->count()}} مهاراة )</span>
                        </h4>
                        <div class="head-decorator head-decorator-xs"></div>
                    </div>
                    <div class="row d-flex">
                        @forelse($content->skills as $con)
                            @include('website.includes.skill')
                        @empty
                            @include('website.layout.no_data')
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="bg-light py-4">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="head mb-5">
                        <h4 class="fw-bold mb-3">
                            @if($content->type == 'lesson')
                                أنشطة الدرس
                            @else
                                أنشطة المحاضرة
                            @endif
                            <span class="text-muted fs-16 ms-2">( {{$content->courses->count()}} أنشطة )</span>
                        </h4>
                        <div class="head-decorator head-decorator-xs"></div>
                    </div>
                    <div class="row d-flex">
                        @forelse($content->courses as $con)
                            @include('website.includes.course')
                        @empty
                            @include('website.layout.no_data')
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="bg-light py-4">
        <div class="container">
            <div class="row">
                @include('website.includes.calendars')
            </div>
        </div>
    </section>

@endsection
