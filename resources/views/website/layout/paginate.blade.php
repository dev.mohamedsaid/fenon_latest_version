<div class="theme-pagination mt-2">
    {!! $content->withQueryString()->links('pagination::bootstrap-4') !!}
</div>
