@extends('admin_dashboard.layout.master')
@section('Page_Title')   اختيارات سؤال | {{$content->calendar?->title}}   @endsection
<style>
    #insert_answers
    {
        background: #bfbfbf;
        margin: 0;
        border-radius: 5px;
        padding: 45px 25px;
        color: #000;
    }
    .oneAnswerBox
    {
        background: white;
        padding: 25px 13px;
        border-radius: 5px;
        border: 2px solid var(--bs-blue);
        margin: 12px;
        width: 47% !important;
    }
    @media screen and (max-width:992px) {
        .oneAnswerBox
        {
            width: 100% !important;
        }
    }
</style>
@section('content')

    <div class="row">
        <div class="col-lg-12 mx-auto">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center">
                        <h5 class="mb-0"> <i class="lni lni-book"></i>    اختيارات سؤال |   <small class="text-warning">({{$content->calendar?->title}})</small> </h5>
                    </div>
                    <div class="row g-3 mt-4">
                        <div class="col-12">
                            <div class="card shadow-none bg-light border">
                                <div class="card-body">
                                    <form class="row g-3" id="validateForm" method="post" enctype="multipart/form-data"
                                          action="{{route('calendar_questions.update', $content->id)}}">
                                        @method('put')
                                        @csrf


                                        <div class="col-12">
                                            <label class="form-label">   التقويم  </label>
                                            <select class="form-control calendar_id"  disabled name="calendar_id" required>
                                                <option value=""> التقويم</option>
                                                @foreach($calendars as $key=>$val)
                                                    <option  @if($val == $content->calendar_id) selected @endif value="{{$val}}">{{$key}}</option>
                                                @endforeach
                                            </select>
                                        </div>



                                        <div class="col-12">
                                            <label class="form-label"> اختر  عملي أو نظري   </label>
                                            <select class="form-control" name="question_kind" disabled id="question_kind" required>
                                                <option @if($content->question_kind == 'theoretical') selected @endif value="theoretical">نظري</option>
                                                <option @if($content->question_kind == 'practical') selected @endif value="practical">عملي</option>
                                            </select>
                                        </div>


                                        <div class="col-12">
                                            <label class="form-label">  السؤال  </label>
                                            <input type="text" disabled value="{{$content->title}}"  class="form-control" required placeholder="ادخل رأس السؤال">
                                        </div>
                                        <div class="col-12">
                                            <label class="form-label">  وصف السؤال  </label>
                                            <input type="text" disabled  value="{{$content->description}}" class="form-control" placeholder="ادخل وصف السؤال">
                                        </div>

                                        @if($content->question_kind == 'theoretical')
                                        <!--Theoretical-->
                                            <div class="col-12 " id="question_theoretical">

                                                <div class="col-12 my-3">
                                                    <label class="form-label">   نوع السؤال   </label>
                                                    <select disabled class="form-control" name="question_type" id="question_type" required>
                                                        <option @if($content->question_type == 'true_false') selected @endif value="true_false">صح أو خطأ</option>
                                                        <option @if($content->question_type == 'single_choice') selected @endif value="single_choice">اختيار فردي (المتعلم يختار اجابة واحدة فقط) </option>
                                                        <option @if($content->question_type == 'multiple_choice') selected @endif value="multiple_choice">اختيار  متعدد (المتعلم يختار أكثر من إجابة)</option>
                                                    </select>
                                                </div>



                                                <div class="col-12 my-3">
{{--                                                    <label class="form-label"> ملف أو صورة للسؤال  <small class="text-danger">Image :(PNG - JPEG - JPG - WEBP - SVG - GIF) File : (PDF - DOC - DOCX - XLSX - XLS)</small> </label>--}}
{{--                                                    <input class="form-control" type="file" name="question_file" />--}}
                                                    @if($content->question_file)
                                                        <img src="{{assetURLFile($content->question_file)}}" width="200px" />
                                                    @endif
                                                </div>


                                            @if(count($content->choices) > 0)
                                                <!--Add Answers-->
                                                    <div class="col-12">
                                                        <div class="row" id="insert_answers">
                                                            <div class="col-12 text-center mb-3">
                                                                <h5> إختيارات السؤال ({{$content->title}})</h5>
                                                            </div>

                                                            @foreach($content->choices as $choice)
                                                                <div class="col-md-6 mb-4 oneAnswerBox">
                                                                    <h6 class="text-center mb-3 fw-bold"> الإختيار  </h6>
                                                                    <div class="my-1 box_of_inputs">
                                                                        <label for="answer1"> الإختيار   </label>
                                                                        <input type="text" disabled class="form-control answer_value" value="{{$choice->choice_text}}" name="choice_text[]" placeholder="اكتب الإختيار " />
                                                                    </div>
                                                                    @if($choice->choice_file)
                                                                    <div class="my-4 box_of_inputs">
                                                                        <label for="answer1">ملف أو صورة الإختيار :    </label>
                                                                        <a download="" class="btn btn-sm btn-success" href="{{assetURLFile($choice->choice_file)}}">ملف {{$choice->choice_file_ext}}</a>
                                                                    </div>
                                                                    @endif
                                                                    @if($choice->choice_video_url)
                                                                    <div class="my-1 box_of_inputs">
                                                                        <label for="answer1">رابط فيديو الإختيار    </label>
                                                                        <input type="url" disabled value="{{$choice->choice_video_url}}"  class="form-control answer_value" name="choice_video[]" />
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                            @endforeach

                                                        </div>
                                                    </div>
                                                @endif



                                            </div>
                                        @elseif($content->question_kind == 'practical')
                                        <!--Practical-->
                                            <div class="col-12" id="question_photopia">

                                            </div>
                                        @endif


                                    </form>
                                </div>
                            </div>
                        </div>
                    </div><!--end row-->
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.5/jquery.validate.min.js" integrity="sha512-rstIgDs0xPgmG6RX1Aba4KV5cWJbAMcvRCVmglpam9SoHZiUCyQVDdH2LPlxoHtrv17XWblE/V/PP+Tr04hbtA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

    <script>
        $(document).ready(function () {
            $("#validateForm").validate({
                rules: {
                    title: {
                        required: true,
                    },
                    short_description: {
                        required: true,
                    },
                    description: {
                        required: true,
                    }

                },
                messages: {
                    title: {
                        required: "الحقل مطلوب",
                    },
                    short_description: {
                        required: "الحقل مطلوب",
                    },
                    description: {
                        required: "الحقل مطلوب",
                    }

                }
            });
        });



    </script>
@endpush
