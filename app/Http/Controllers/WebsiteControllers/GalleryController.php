<?php

namespace App\Http\Controllers\WebsiteControllers;

use App\Http\Controllers\Controller;
use App\Models\Gallery;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    //Index
    public function index()
    {
        $page_title = 'المعارض الفنية';
        $content = Gallery::whereStatus('yes')->orderBy('sort', 'asc')->paginate(config('app.paginate'));
        return view('website.galleries', compact('content','page_title'));
    }

    public function show($id)
    {
        $content = Gallery::with('images')->whereStatus('yes')->whereId($id)->first();
        if(!$content)
        {
            return view('errors.404');
        }
        $page_title = $content->title;

        $galleries = Gallery::with('images')->where('id', '!=',$content->id)->whereStatus('yes')->latest()->limit(3)->get();

        return view('website.gallery', compact('content','page_title','galleries'));
    }


}
