<?php

namespace App\Http\Controllers\TeacherControllers;

use App\Http\Controllers\Controller;
use App\Http\Traits\HelperTrait;
use App\Models\Level;
use App\Models\ModuleFile;
use Illuminate\Http\Request;
use App\Http\Requests\CurriculumRequest;
use App\Models\Curriculum;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CurriculumController extends Controller
{
    use HelperTrait;

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $content = Curriculum::with('level')->orderBy('sort', 'asc')->paginate($this->paginate);
        return view('admin_dashboard.curriculums.index' , compact('content'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $levels = Level::whereStatus('yes')->orderBy('sort', 'asc')->pluck('id', 'title');
        return view('admin_dashboard.curriculums.create', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CurriculumRequest $request)
    {
        $data = $request->validated();
        DB::beginTransaction();
        try {
            //upload Image
            $image = $this->upload_file_helper_trait($request,'image', 'uploads/');
            $data['image'] = $image;
            isset($data['status']) ? $data['status']='yes' : $data['status'] = 'no';
            $created = Curriculum::create($data);
            //Save Multiple Files
            $this->saveMultipleFiles('Curriculum', $created->id, $data,'uploads/');
            DB::commit();
            toastr()->success($this->insertMsg, 'نجح', ['timeOut' => 5000]);
            return redirect()->back();
        } catch (\Exception $e) {
            DB::rollback();
            toastr()->error($this->error, 'فشل', ['timeOut' => 5000]);
            return redirect()->back();
        }
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Curriculum $curriculum)
    {
        $content =  $curriculum;
        $levels = Level::whereStatus('yes')->orderBy('sort', 'asc')->pluck('id', 'title');
        return view('admin_dashboard.curriculums.edit', compact('content','levels'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CurriculumRequest $request, Curriculum $curriculum)
    {
        $data = $request->validated();
        if($request->hasFile('image')){
            $image = $this->upload_file_helper_trait($request,'image', 'uploads/');
            $data['image'] = $image;
        }
        isset($data['status']) ? $data['status']='yes' : $data['status'] = 'no';
        $curriculum->update($data);
        if($request->hasFile('file_uploaded'))
        {
            $this->saveMultipleFiles('Curriculum', $curriculum->id, $data,'uploads/');
        }
        toastr()->success($this->updateMsg, 'نجح', ['timeOut' => 5000]);
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Curriculum $curriculum)
    {
        $curriculum->files()->delete();
        $curriculum->delete();
        toastr()->success($this->deleteMsg, 'نجح', ['timeOut' => 5000]);
        return redirect()->back();
    }


    //moduleFileDestroy
    public function moduleFileDestroy($id)
    {
        $item = ModuleFile::find($id);
        $item->delete();
        toastr()->success(' تم حذف العنصر بنجاح', 'نجح', ['timeOut' => 5000]);
        return redirect()->back();
    }


}
