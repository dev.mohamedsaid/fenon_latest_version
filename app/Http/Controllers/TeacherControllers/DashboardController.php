<?php

namespace App\Http\Controllers\TeacherControllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\StudentRequest;
use App\Http\Traits\HelperTrait;
use App\Models\Category;
use App\Models\Course;
use App\Models\User;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    use HelperTrait;

    public function dashboard()
    {
        $students = User::whereType(3)->count();
        $teachers = User::whereType(2)->count();
        $courses = Course::whereStatus('yes')->where('teacher_id', $this->userId())->count();
        $categories = Category::whereStatus('yes')->where('teacher_id', $this->userId())->count();
        return view('website.teachers.dashboard.dashboard', compact('students', 'teachers','courses','categories'));
    }



}


