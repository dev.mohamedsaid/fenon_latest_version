<?php

/*
 * This file is part of the yoeunes/toastr package.
 * (c) Younes KHOUBZA <younes.khoubza@gmail.com>
 */

return array(
    'scripts' => array(
        'cdn' => array(
            'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.3/jquery.min.js',
            'https://www.unpkg.com/@flasher/flasher@1.2.4/dist/flasher.min.js',
            'https://www.unpkg.com/@flasher/flasher-toastr@1.2.4/dist/flasher-toastr.min.js',
        ),
        'local' => array(
            '/vendor/flasher/jquery.min.js',
            '/vendor/flasher/flasher-toastr.min.js',
        ),
    ),
);
